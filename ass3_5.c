#include<stdio.h>

long long int merge(long long int a[],long long int le[],long long int ri[],long long int nl,long long int nr)
{
//	int nl=length/2;
//	int nr=length-nl;
	long long int i=0;
	long long int l=0;
	long long int r=0;
	while(l<nl && r<nr)
	{
		if(le[l]<ri[r])
		{
			a[i]=le[l];
			l++;
			i++;
		}
		else
		{
			a[i]=ri[r];
			r++;
			i++;
		}
	}
	while(l<nl)
	{
		a[i]=le[l];
		l++;
                i++;

	}
	while(r<nr)
        {
                a[i]=ri[r];
                r++;
                i++;
        }
	return 0;
}

long long int mergesort(long long int arr[],long long int len)
{
	long long int i;
	long long int mid=len/2;
	long long int left[1000002];
	long long int right[1000002];

	if(len==1)
		return 0;
	else
 	{
		for(i=0;i<mid;i++)
			left[i]=arr[i];
		for(i=mid;i<len;i++)
			right[i-mid]=arr[i];
		mergesort(left,mid);
		mergesort(right,len-mid);
		merge(arr,left,right,mid,len-mid);
		return 0;
	}
}

int main()
{
	long long int n,i;
	scanf("%lld",&n);
	long long int power[1000002];
	for(i=0;i<n;i++)
	{
		scanf("%lld",&power[i]);
	}
	//sort array using merge sort
	mergesort(power,n);	
	long long int sum=0;
	for(i=0;i<n;i++)
		sum=sum+power[i];
	long long int sum1=power[n-1];
	long long int count=1;
	for(i=n-2;i>=0;i--)
	{
		if(sum1>sum-sum1)
		{
			break;
		}
		else
			sum1+=power[i];
		count++;
	}

	printf("%lld\n",count);
	return 0;
}
